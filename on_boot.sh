#! /bin/bash

sleep 7 

chvt 2

echo "Enter the full hostname of the machine"
read -p "Hostname: " hn

hostnamectl set-hostname $hn

read -p "Enter the Puppet role: " -e -i "role::ubuntu_desktop" role

mkdir -p /etc/puppetlabs/facter/facts.d

echo "role=$role" > /etc/puppetlabs/facter/facts.d/role.txt

salt='securityishard'

psk=$(echo "$hn$salt" | sha512sum | rev | cut -c 4- | rev)

curl -k https://penelope.its.unimelb.edu.au:8140/packages/current/install.bash | sudo bash -s agent:certname=$hn custom_attributes:challengePassword=$psk

systemctl disable set-hostname.service

chvt 7
